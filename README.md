# wall24

![wall24 logo](logo.png)

`wall24` is a 24-hour "dynamic-ish" wallpaper cycler for Linux (X11).

## Overview

- `wall24` runs a persistent script using `feh` to cycle through a set of wallpapers for every 24 hours.
- It relies on 24 image file paths stored in `$HOME/.config/wall24/<theme>/config`.

  ```file
  <path-to-image-0>
  <path-to-image-1>
  ...
  <path-to-image-23>
  ```

- Starting with the current hour and at every local clock hour, `wall24` cycles to the image listed in this `config` at each hour's index.
- The same image path can be used for multiple hours.

### Dependencies

- [feh](https://feh.finalrewind.org/)
- [wget](https://www.gnu.org/software/wget/)

### Themes

- `catalina`: MacOS Catalina

  ![Catalina Preview Not Found!](https://i.imgur.com/j0BevCS.jpg)

- `mojave`: MacOS Mojave

  ![Mojave Preview Not Found!](https://i.imgur.com/iMd0ey0.jpg)

### Setup

- Run `setup.sh <theme>` to set up your `<theme>` for **the first time**.

  ```sh
  sh setup.sh <theme>
  ```

  For example, `sh setup.sh mojave`.

- If you want to quickly test it, run `test.sh <theme>`, preferably in a shell so you can watch the time.

  ```sh
  sh test.sh <theme>
  ```

- Put `wall24` somewhere in your `PATH` and make it executable. For example,

  ```sh
  cp wall24 /usr/bin
  chmod +x /usr/bin/wall24
  ```

### Usage

For 24-hour cycling, run `wall24`.

```sh
wall24 <theme>
```

### Customize

If you'd like, you're free to edit the config files in `~/.config/wall24` and/or load your own images/themes. Simply follow the example of already set-up themes.

## Development/Pages

- [**Homepage**](https://leglesslamb.gitlab.io/post/wall24)
- [**GitLab**](https://gitlab.com/leglesslamb/wall24)
  - Working issues and Merge Requests (MRs) are reviewed.
  - Bug reports and feature requests are preferred.
- [**GitHub (Mirror)**](https://github.com/leglesslamb/wall24)
  - Bug reports and feature requests are accepted.

## Acknowledgements

- [WinDynamicDesktop (WDD) Themes](https://windd.info/themes) for supplying free wallpaper sets.
- [jcbermu](https://unix.stackexchange.com/users/94603/jcbermu) for [this shell snippet](https://unix.stackexchange.com/questions/213288/killing-the-previous-instances-of-a-script-before-running-the-same-unix-script).
