#!/bin/sh

echo "wall24: quickly testing $1..."
echo
echo "Time   Image"

index=0
for img in $(cat $HOME/.config/wall24/$1/config); do
    file=$HOME/$img
    feh --bg-scale $file
    echo "$(printf "%02d:00" $index)  $file"
    sleep 1s
    index=$(($index+1))
done
